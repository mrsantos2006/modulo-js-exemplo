
import funcao from "./funcoes.js";

function iniciar_resultado(){
    document.getElementById("resultado").innerText = "..."
}

function clique_me(){
    
    var texto_digitado = document.getElementById("texto").value.toString().trim();
    
    if(!(texto_digitado === '')) {
       document.getElementById("resultado").innerText = funcao(texto_digitado);
    } else {
        iniciar_resultado();
    }


}

iniciar_resultado();

document.getElementById("botao").addEventListener('click', () => {
    clique_me();
});

document.getElementById("texto").addEventListener('keypress', function (e) {
    if (e.key === 'Enter') clique_me();
    
});